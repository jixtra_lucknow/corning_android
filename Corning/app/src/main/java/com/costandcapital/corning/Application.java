package com.costandcapital.corning;

/**
 * Created by prakharsingh on 18/04/16.
 */
public class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FontsOverride.setDefaultFont(this, "MONOSPACE", "TheSans Plain.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "Galano Grotesque Light.otf");
    }
}
