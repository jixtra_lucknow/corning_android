package com.costandcapital.corning.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.costandcapital.corning.R;
import com.costandcapital.corning.Utils.CorningUtils;
import com.costandcapital.corning.Views.CorningHeaderView;
import com.costandcapital.corning.Views.InformationAlertView;
import com.costandcapital.corning.Views.ResultTextViewRowLayout;
import com.costandcapital.corning.Views.SingleEditTextRowLayout;

public class PaymentCalculatorActivity extends AppCompatActivity implements View.OnClickListener{

    public CorningHeaderView corningHeaderView;

    public SingleEditTextRowLayout rowAnnualSpend;
    public SingleEditTextRowLayout rowCurrentStateDays;
    public ResultTextViewRowLayout rowBeforeResult;
    public SingleEditTextRowLayout rowFutureStateDays;
    public ResultTextViewRowLayout rowAfterResult;
    public ResultTextViewRowLayout rowFlowImpact;

    static String ANNUAL_SPEND = "1000000";
    static String CURRENT_STATE_DAYS = "30";
    static String FUTURE_STATE_DAYS = "75";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_calculator);
        getSupportActionBar().hide();
        this.corningHeaderView = (CorningHeaderView) findViewById(R.id.corningHeaderView);
        rowAnnualSpend = (SingleEditTextRowLayout)findViewById(R.id.row1);
        rowCurrentStateDays = (SingleEditTextRowLayout)findViewById(R.id.row2);
        rowBeforeResult = (ResultTextViewRowLayout)findViewById(R.id.row3);
        rowFutureStateDays = (SingleEditTextRowLayout)findViewById(R.id.row4);
        rowAfterResult = (ResultTextViewRowLayout)findViewById(R.id.row5);
        rowFlowImpact = (ResultTextViewRowLayout)findViewById(R.id.row6);

        rowAnnualSpend.txtVwRowTitle.setText(getResources().getString(R.string.annual_spend_with_supplier));
        rowCurrentStateDays.txtVwRowTitle.setText(getResources().getString(R.string.no_of_days_current_state));
        rowBeforeResult.txtVwRowTitle.setText(getResources().getString(R.string.before_results));
        rowFutureStateDays.txtVwRowTitle.setText(getResources().getString(R.string.no_of_days_future_state));
        rowAfterResult.txtVwRowTitle.setText(getResources().getString(R.string.after_results));
        rowFlowImpact.txtVwRowTitle.setText(getResources().getString(R.string.net_cash_flow_impact));
        this.corningHeaderView.txtVwTitleHeader.setText(getResources().getString(R.string.payment_term));
        this.corningHeaderView.btnLeftToolBar.setBackground(getResources().getDrawable(R.drawable.discount_button));
        rowFlowImpact.txtVwRowTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        rowFlowImpact.txtVwRowValue.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);

        this.corningHeaderView.btnLeftToolBar.setOnClickListener(this);
        this.corningHeaderView.btnRightToolBar.setOnClickListener(this);

        EditText.OnEditorActionListener onEditorActionListener = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    if(rowAnnualSpend.edtTxtRowValue.hasFocus()){
                        imm.hideSoftInputFromWindow(rowAnnualSpend.edtTxtRowValue.getWindowToken(), 0);
                        rowAnnualSpend.edtTxtRowValue.clearFocus();
                    }else if(rowCurrentStateDays.edtTxtRowValue.hasFocus()){
                        imm.hideSoftInputFromWindow(rowCurrentStateDays.edtTxtRowValue.getWindowToken(), 0);
                        rowCurrentStateDays.edtTxtRowValue.clearFocus();
                    }else if(rowFutureStateDays.edtTxtRowValue.hasFocus()){
                        imm.hideSoftInputFromWindow(rowFutureStateDays.edtTxtRowValue.getWindowToken(), 0);
                        rowFutureStateDays.edtTxtRowValue.clearFocus();
                    }
                    return true;
                }
                return false;
            }
        };

        View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    String filterString = rowCurrentStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowCurrentStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));

                    filterString = rowFutureStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowFutureStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));

                    filterString = rowAnnualSpend.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowAnnualSpend.edtTxtRowValue.setText(CorningUtils.getConvertedValueForCurrency(filterString, false));
                    return;
                }

                final EditText currentView = (EditText) v;
                if(currentView.equals(rowAnnualSpend.edtTxtRowValue)){
                    String filterString = rowAnnualSpend.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowAnnualSpend.edtTxtRowValue.setText(filterString);

                    filterString = rowCurrentStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowCurrentStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));

                    filterString = rowFutureStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowFutureStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));
                }else if(currentView.equals(rowCurrentStateDays.edtTxtRowValue)){
                    String filterString = rowAnnualSpend.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowAnnualSpend.edtTxtRowValue.setText(CorningUtils.getConvertedValueForCurrency(filterString, false));

                    filterString = rowCurrentStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowCurrentStateDays.edtTxtRowValue.setText(filterString);

                    filterString = rowFutureStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowFutureStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));
                }else if(currentView.equals(rowFutureStateDays.edtTxtRowValue)){
                    String filterString = rowAnnualSpend.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowAnnualSpend.edtTxtRowValue.setText(CorningUtils.getConvertedValueForCurrency(filterString, false));

                    filterString = rowCurrentStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowCurrentStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));

                    filterString = rowFutureStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowFutureStateDays.edtTxtRowValue.setText(filterString);
                }

                currentView.post(new Runnable() {
                    @Override
                    public void run() {
                        currentView.setSelection(currentView.getText().length());
                    }
                });
            }
        };

        rowAnnualSpend.edtTxtRowValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                ANNUAL_SPEND = s.toString();
                calculatePaymentTerm();
            }
        });
        rowCurrentStateDays.edtTxtRowValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                CURRENT_STATE_DAYS = s.toString();
                calculatePaymentTerm();
            }
        });
        rowFutureStateDays.edtTxtRowValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                FUTURE_STATE_DAYS = s.toString();
                calculatePaymentTerm();
            }
        });

        rowAnnualSpend.edtTxtRowValue.setOnFocusChangeListener(onFocusChangeListener);
        rowCurrentStateDays.edtTxtRowValue.setOnFocusChangeListener(onFocusChangeListener);
        rowFutureStateDays.edtTxtRowValue.setOnFocusChangeListener(onFocusChangeListener);

        rowAnnualSpend.edtTxtRowValue.setOnEditorActionListener(onEditorActionListener);
        rowCurrentStateDays.edtTxtRowValue.setOnEditorActionListener(onEditorActionListener);
        rowFutureStateDays.edtTxtRowValue.setOnEditorActionListener(onEditorActionListener);

        //default value
        rowAnnualSpend.edtTxtRowValue.setText(CorningUtils.getConvertedValueForCurrency(ANNUAL_SPEND,false));
        rowAnnualSpend.edtTxtRowValue.setInputType(InputType.TYPE_CLASS_NUMBER);
        rowCurrentStateDays.edtTxtRowValue.setText(CURRENT_STATE_DAYS);
        rowCurrentStateDays.edtTxtRowValue.setInputType(InputType.TYPE_CLASS_NUMBER);
        rowFutureStateDays.edtTxtRowValue.setText(FUTURE_STATE_DAYS);
        rowFutureStateDays.edtTxtRowValue.setInputType(InputType.TYPE_CLASS_NUMBER);
    }

    private void calculatePaymentTerm(){
        String annualSpendString = rowAnnualSpend.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$", "").replace(" ", "");
        String currentDaysString = rowCurrentStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$", "").replace(" ", "");
        String futureDaysString = rowFutureStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$", "").replace(" ", "");

        double annualSpendValue = 0;
        double currentDaysValue = 0;
        double futureDaysSpendValue = 0;

        try{
            annualSpendValue = Double.parseDouble(annualSpendString);
        }catch (Exception e){
            annualSpendValue = 0;
        }

        try{
            currentDaysValue = Double.parseDouble(currentDaysString);
        }catch (Exception e){
            currentDaysValue = 0;
        }

        try{
            futureDaysSpendValue = Double.parseDouble(futureDaysString);
        }catch (Exception e){
            futureDaysSpendValue = 0;
        }

        double beforeResult;
        double afterResult;
        double netFlowImpact;

        try{
            beforeResult = (annualSpendValue * currentDaysValue * 0.1)/365;
        }catch (Exception e){
            beforeResult = 0;
        }

        try{
            afterResult = (annualSpendValue * futureDaysSpendValue * 0.1)/365;
        }catch (Exception e){
            afterResult = 0;
        }

        try{
            netFlowImpact = afterResult - beforeResult;
        }catch (Exception e){
            netFlowImpact = 0;
        }

        rowBeforeResult.txtVwRowValue.setText(CorningUtils.getConvertedValueForCurrency(beforeResult+"", true));
        rowAfterResult.txtVwRowValue.setText(CorningUtils.getConvertedValueForCurrency(afterResult + "", true));
        rowFlowImpact.txtVwRowValue.setText(CorningUtils.getConvertedValueForCurrency(netFlowImpact + "", true));
        //set stroke
        rowBeforeResult.txtVwRowValue.setStrokeWidth(0);
        rowAfterResult.txtVwRowValue.setStrokeWidth(0);

        if(netFlowImpact < 0){
            rowFlowImpact.txtVwRowValue.setTextColor(getResources().getColor(R.color.NegativeValue));
            rowFlowImpact.txtVwRowValue.setStrokeWidth(0);
        }else{
            rowFlowImpact.txtVwRowValue.setTextColor(getResources().getColor(R.color.PositiveValue));
            rowFlowImpact.txtVwRowValue.setStrokeWidth(0);
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Do you want to quit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                }).setNegativeButton("No", null).show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLeftToolBar:{
                startActivity(new Intent(PaymentCalculatorActivity.this, DiscountCalculatorActivity.class));
                finish();
            }break;
            case R.id.btnRightToolBar:{
                InformationAlertView informationAlertView = new InformationAlertView(this);
                informationAlertView.show();
            }break;
        }
    }

}
