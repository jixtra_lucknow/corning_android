package com.costandcapital.corning.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.costandcapital.corning.R;

/**
 * Created by prakharsingh on 16/04/16.
 */
public class CorningHeaderView extends RelativeLayout{

    public TextView btnLeftToolBar;
    public TextView btnRightToolBar;
    public TextView txtVwTitleHeader;

    public CorningHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_corning_header,this, true);

        this.btnLeftToolBar = (TextView) view.findViewById(R.id.btnLeftToolBar);
        this.btnRightToolBar = (TextView) view.findViewById(R.id.btnRightToolBar);
        this.txtVwTitleHeader = (TextView) view.findViewById(R.id.txtVwTitleHeader);
    }

}
