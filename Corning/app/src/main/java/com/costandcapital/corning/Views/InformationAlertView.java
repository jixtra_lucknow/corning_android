package com.costandcapital.corning.Views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.costandcapital.corning.R;

/**
 * Created by prakharsingh on 16/04/16.
 */
public class InformationAlertView extends Dialog implements View.OnClickListener{

    public Context context;
    public TextView txtVwAlertViewButton;
    public TextView txtVwAlertViewMessage;
    String HTML_PAGE = "<p>This tool is designed to:</p><p>1. Evaluate changes in payment and discount term scenarios to determine the net impact to cash flow</p><p>2. Compare \"Current State\" to \"Future State\"</p><p>3. Develop negotiation strategies</p><p>Example: What scenarios of payment terms or discount terms would be cash neutral to the Company, What Various scenarios of payment terms or discount terms would maximize the Company's cash flow.</p><h4><u>Instructions</u></h4><p>1. Enter data into the appropriate cells highlighted in yellow.</p><p>2. \"Current State\" designates the current situation - payment and/or discount term.</p><p>3. \"Future State\" designates the proposed changes - payment and/or discount term.</p><p>4. \"Net Cash Flow Impact\" calculates the net change in  cash flow going from the \"Current State\" to the \"Future State\". A positive number indicates a positive impact to the Company's cash flow. A negative number indicates a negative impact to the Company's cash flow.</p><p>5. We have chosen annual cost of capital to be 10% for the calculations.</p>";

    public InformationAlertView(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.view_information_dialog);

        txtVwAlertViewMessage = (TextView) findViewById(R.id.txtVwAlertViewMessage);
        txtVwAlertViewButton = (TextView) findViewById(R.id.txtVwAlertViewButton);
        txtVwAlertViewButton.setOnClickListener(this);
        txtVwAlertViewMessage.setText(Html.fromHtml(HTML_PAGE));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txtVwAlertViewButton:{
                this.dismiss();
            }break;
        }
    }
}
