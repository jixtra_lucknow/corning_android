package com.costandcapital.corning.Utils;

import java.text.DecimalFormat;

/**
 * Created by prakharsingh on 19/04/16.
 */
public class CorningUtils {
    public static String getConvertedValueForDiscount(String value){
        String filterString = value.replace("%","").replace(" ","").replace(",","");
        double amount;
        try{
            amount = Double.parseDouble(filterString);
        }catch(Exception e){
            amount = 0;
        }

        DecimalFormat formatter1 = new DecimalFormat("#,###.00");
        if(amount == 0){
            return "0" + formatter1.format(amount) + "%";
        }else{
            return formatter1.format(amount) + "%";
        }

    }

    public static String getConvertedValueForCurrency(String value, boolean decimal){
        String filterString = value.replace(",","").replace("$","").replace(" ","");
        double amount;
        try{
            amount = Double.parseDouble(filterString);
        }catch(Exception e){
            amount = 0;
        }
        DecimalFormat formatter1 = decimal ? new DecimalFormat("#,###.00") : new DecimalFormat("#,###");

        if(amount == 0 && decimal){
            return  "$ "+ "0" + formatter1.format(amount);
        }else{
            return "$ "+ formatter1.format(amount);
        }
    }

    public static String getConvertedValueForNumber(String value, boolean decimal){
        String filterString = value.replace(",","").replace("$","").replace(" ","");
        double amount;
        try{
            amount = Double.parseDouble(filterString);
        }catch(Exception e){
            amount = 0;
        }
        DecimalFormat formatter1 = decimal ? new DecimalFormat("#,###.00") : new DecimalFormat("#,###");

        if(amount == 0 && decimal){
            return "0" + formatter1.format(amount);
        }else{
            return formatter1.format(amount);
        }
    }



}
