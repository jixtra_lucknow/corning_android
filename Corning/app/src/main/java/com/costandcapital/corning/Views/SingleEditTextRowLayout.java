package com.costandcapital.corning.Views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.costandcapital.corning.R;

/**
 * Created by prakharsingh on 16/04/16.
 */
public class SingleEditTextRowLayout extends RelativeLayout{

    public TextView txtVwRowTitle;
    public EditText edtTxtRowValue;

    public SingleEditTextRowLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_single_edit_text_row_layout,this, true);

        this.txtVwRowTitle = (TextView) view.findViewById(R.id.txtVwRowTitle);
        this.edtTxtRowValue = (EditText) view.findViewById(R.id.edtTxtRowValue);
    }
}
