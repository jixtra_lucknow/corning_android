package com.costandcapital.corning.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.costandcapital.corning.R;
import com.costandcapital.corning.Utils.CorningUtils;
import com.costandcapital.corning.Views.CorningHeaderView;
import com.costandcapital.corning.Views.InformationAlertView;
import com.costandcapital.corning.Views.ResultTextViewRowLayout;
import com.costandcapital.corning.Views.SingleEditTextRowLayout;

public class DiscountCalculatorActivity extends AppCompatActivity implements View.OnClickListener{

    private CorningHeaderView corningHeaderView;
    private SingleEditTextRowLayout rowAnnualSpend;
    private SingleEditTextRowLayout rowCurrentStateDays;
    private SingleEditTextRowLayout row2DiscountCurrentState;
    private ResultTextViewRowLayout rowSubtotal1;
    private ResultTextViewRowLayout rowDiscountImpact1;
    private ResultTextViewRowLayout rowBeforeNetImpact;
    private SingleEditTextRowLayout rowFutureStateDays;
    private SingleEditTextRowLayout rowDiscountFutureState;
    private ResultTextViewRowLayout rowSubtotal2;
    private ResultTextViewRowLayout rowDiscountImpact3;
    private ResultTextViewRowLayout rowAfterNetImpact;
    private ResultTextViewRowLayout rowNetCashFlow;

    static String ANNUAL_SPEND = "1000000";
    static String CURRENT_STATE_DAYS = "75";
    static String CURRENT_STATE_PERCENTAGE = "0";
    static String FUTURE_STATE_DAYS = "10";
    static String FUTURE_STATE_PERCENTAGE = "2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discount_calculator);
        getSupportActionBar().hide();
        this.corningHeaderView = (CorningHeaderView) findViewById(R.id.corningHeaderView);
        rowAnnualSpend = (SingleEditTextRowLayout) findViewById(R.id.row1);
        rowCurrentStateDays = (SingleEditTextRowLayout) findViewById(R.id.row2_item1);
        row2DiscountCurrentState = (SingleEditTextRowLayout) findViewById(R.id.row2_item2);
        rowSubtotal1 = (ResultTextViewRowLayout) findViewById(R.id.row3_item1);
        rowDiscountImpact1 = (ResultTextViewRowLayout) findViewById(R.id.row3_item2);
        rowBeforeNetImpact = (ResultTextViewRowLayout) findViewById(R.id.row4);
        rowFutureStateDays = (SingleEditTextRowLayout) findViewById(R.id.row5_item1);
        rowDiscountFutureState = (SingleEditTextRowLayout) findViewById(R.id.row5_item2);
        rowSubtotal2 = (ResultTextViewRowLayout) findViewById(R.id.row6_item1);
        rowDiscountImpact3 = (ResultTextViewRowLayout) findViewById(R.id.row6_item2);
        rowAfterNetImpact = (ResultTextViewRowLayout) findViewById(R.id.row7);
        rowNetCashFlow = (ResultTextViewRowLayout) findViewById(R.id.row8);

        //Set row titles
        rowAnnualSpend.txtVwRowTitle.setText(getResources().getString(R.string.annual_spend_with_supplier));
        rowCurrentStateDays.txtVwRowTitle.setText(getResources().getString(R.string.no_of_days_current_state));
        row2DiscountCurrentState.txtVwRowTitle.setText(getResources().getString(R.string.discount_pct_current_state));
        row2DiscountCurrentState.txtVwRowTitle.setGravity(Gravity.RIGHT);
        rowSubtotal1.txtVwRowTitle.setText(getResources().getString(R.string.subtotal_current));
        rowDiscountImpact1.txtVwRowTitle.setText(getResources().getString(R.string.discount_impact_current));
        rowDiscountImpact1.txtVwRowTitle.setGravity(Gravity.RIGHT);
        rowBeforeNetImpact.txtVwRowTitle.setText(getResources().getString(R.string.before_net_impact));
        rowFutureStateDays.txtVwRowTitle.setText(getResources().getString(R.string.no_of_days_future_state));
        rowDiscountFutureState.txtVwRowTitle.setText(getResources().getString(R.string.discount_pct_future_state));
        rowDiscountFutureState.txtVwRowTitle.setGravity(Gravity.RIGHT);
        rowSubtotal2.txtVwRowTitle.setText(getResources().getString(R.string.subtotal_future));
        rowDiscountImpact3.txtVwRowTitle.setText(getResources().getString(R.string.discount_impact_future));
        rowDiscountImpact3.txtVwRowTitle.setGravity(Gravity.RIGHT);
        rowAfterNetImpact.txtVwRowTitle.setText(getResources().getString(R.string.after_net_impact));
        rowNetCashFlow.txtVwRowTitle.setText(getResources().getString(R.string.net_cash_flow_impact));
        this.corningHeaderView.txtVwTitleHeader.setText(getResources().getString(R.string.discount_term));
        this.corningHeaderView.btnLeftToolBar.setBackground(getResources().getDrawable(R.drawable.payment_button));

        rowNetCashFlow.txtVwRowTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        rowNetCashFlow.txtVwRowValue.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);

        this.corningHeaderView.btnLeftToolBar.setOnClickListener(this);
        this.corningHeaderView.btnRightToolBar.setOnClickListener(this);

        EditText.OnEditorActionListener onEditorActionListener = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    if(rowAnnualSpend.edtTxtRowValue.hasFocus()){
                        imm.hideSoftInputFromWindow(rowAnnualSpend.edtTxtRowValue.getWindowToken(), 0);
                        rowAnnualSpend.edtTxtRowValue.clearFocus();
                    }else if(rowCurrentStateDays.edtTxtRowValue.hasFocus()){
                        imm.hideSoftInputFromWindow(rowCurrentStateDays.edtTxtRowValue.getWindowToken(), 0);
                        rowCurrentStateDays.edtTxtRowValue.clearFocus();
                    }else if(rowFutureStateDays.edtTxtRowValue.hasFocus()){
                        imm.hideSoftInputFromWindow(rowFutureStateDays.edtTxtRowValue.getWindowToken(), 0);
                        rowFutureStateDays.edtTxtRowValue.clearFocus();
                    }else if(row2DiscountCurrentState.edtTxtRowValue.hasFocus()){
                        imm.hideSoftInputFromWindow(rowFutureStateDays.edtTxtRowValue.getWindowToken(), 0);
                        row2DiscountCurrentState.edtTxtRowValue.clearFocus();
                    }else if(rowDiscountFutureState.edtTxtRowValue.hasFocus()){
                        imm.hideSoftInputFromWindow(rowFutureStateDays.edtTxtRowValue.getWindowToken(), 0);
                        rowDiscountFutureState.edtTxtRowValue.clearFocus();
                    }
                    return true;
                }
                return false;
            }
        };

        View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    String filterString = rowAnnualSpend.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowAnnualSpend.edtTxtRowValue.setText(CorningUtils.getConvertedValueForCurrency(filterString, false));

                    filterString = row2DiscountCurrentState.edtTxtRowValue.getEditableText().toString().replace("%", "").replace(" ","").replace(",", "");
                    row2DiscountCurrentState.edtTxtRowValue.setText(CorningUtils.getConvertedValueForDiscount(filterString));

                    filterString = rowDiscountFutureState.edtTxtRowValue.getEditableText().toString().replace("%", "").replace(" ","").replace(",", "");
                    rowDiscountFutureState.edtTxtRowValue.setText(CorningUtils.getConvertedValueForDiscount(filterString));

                    filterString = rowCurrentStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowCurrentStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));

                    filterString = rowFutureStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowFutureStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));

                    return;
                }

                final EditText currentView = (EditText) v;
                if(currentView.equals(rowAnnualSpend.edtTxtRowValue)){
                    String filterString = rowAnnualSpend.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowAnnualSpend.edtTxtRowValue.setText(filterString);

                    filterString = row2DiscountCurrentState.edtTxtRowValue.getEditableText().toString().replace("%", "").replace(" ","").replace(",", "");
                    row2DiscountCurrentState.edtTxtRowValue.setText(CorningUtils.getConvertedValueForDiscount(filterString));

                    filterString = rowDiscountFutureState.edtTxtRowValue.getEditableText().toString().replace("%", "").replace(" ","").replace(",", "");
                    rowDiscountFutureState.edtTxtRowValue.setText(CorningUtils.getConvertedValueForDiscount(filterString));

                    filterString = rowCurrentStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowCurrentStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));

                    filterString = rowFutureStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowFutureStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));
                }else if(currentView.equals(rowCurrentStateDays.edtTxtRowValue)){
                    String filterString = rowAnnualSpend.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowAnnualSpend.edtTxtRowValue.setText(CorningUtils.getConvertedValueForCurrency(filterString, false));

                    filterString = row2DiscountCurrentState.edtTxtRowValue.getEditableText().toString().replace("%", "").replace(" ","").replace(",", "");
                    row2DiscountCurrentState.edtTxtRowValue.setText(CorningUtils.getConvertedValueForDiscount(filterString));

                    filterString = rowDiscountFutureState.edtTxtRowValue.getEditableText().toString().replace("%", "").replace(" ","").replace(",", "");
                    rowDiscountFutureState.edtTxtRowValue.setText(CorningUtils.getConvertedValueForDiscount(filterString));

                    filterString = rowCurrentStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowCurrentStateDays.edtTxtRowValue.setText(filterString);

                    filterString = rowFutureStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowFutureStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));
                }else if(currentView.equals(row2DiscountCurrentState.edtTxtRowValue)){
                    String filterString = rowAnnualSpend.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowAnnualSpend.edtTxtRowValue.setText(CorningUtils.getConvertedValueForCurrency(filterString, false));

                    filterString = row2DiscountCurrentState.edtTxtRowValue.getEditableText().toString().replace("%", "").replace(" ","").replace(",", "");
                    row2DiscountCurrentState.edtTxtRowValue.setText(filterString);

                    filterString = rowDiscountFutureState.edtTxtRowValue.getEditableText().toString().replace("%", "").replace(" ","").replace(",", "");
                    rowDiscountFutureState.edtTxtRowValue.setText(CorningUtils.getConvertedValueForDiscount(filterString));

                    filterString = rowCurrentStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowCurrentStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));

                    filterString = rowFutureStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowFutureStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));
                }else if(currentView.equals(rowFutureStateDays.edtTxtRowValue)){
                    String filterString = rowAnnualSpend.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowAnnualSpend.edtTxtRowValue.setText(CorningUtils.getConvertedValueForCurrency(filterString, false));

                    filterString = row2DiscountCurrentState.edtTxtRowValue.getEditableText().toString().replace("%", "").replace(" ","").replace(",", "");
                    row2DiscountCurrentState.edtTxtRowValue.setText(CorningUtils.getConvertedValueForDiscount(filterString));

                    filterString = rowDiscountFutureState.edtTxtRowValue.getEditableText().toString().replace("%", "").replace(" ","").replace(",", "");
                    rowDiscountFutureState.edtTxtRowValue.setText(CorningUtils.getConvertedValueForDiscount(filterString));

                    filterString = rowCurrentStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowCurrentStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));

                    filterString = rowFutureStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowFutureStateDays.edtTxtRowValue.setText(filterString);
                }else if(currentView.equals(rowDiscountFutureState.edtTxtRowValue)){
                    String filterString = rowAnnualSpend.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowAnnualSpend.edtTxtRowValue.setText(CorningUtils.getConvertedValueForCurrency(filterString, false));

                    filterString = row2DiscountCurrentState.edtTxtRowValue.getEditableText().toString().replace("%", "").replace(" ","").replace(",", "");
                    row2DiscountCurrentState.edtTxtRowValue.setText(CorningUtils.getConvertedValueForDiscount(filterString));

                    filterString = rowDiscountFutureState.edtTxtRowValue.getEditableText().toString().replace("%", "").replace(" ","").replace(",", "");
                    rowDiscountFutureState.edtTxtRowValue.setText(filterString);

                    filterString = rowCurrentStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowCurrentStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));

                    filterString = rowFutureStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$","").replace(" ","");
                    rowFutureStateDays.edtTxtRowValue.setText(CorningUtils.getConvertedValueForNumber(filterString, false));
                }

                currentView.post(new Runnable() {
                    @Override
                    public void run() {
                        currentView.setSelection(currentView.getText().length());
                    }
                });
            }
        };

        rowAnnualSpend.edtTxtRowValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                ANNUAL_SPEND = s.toString();
                calculateDiscountTerm();
            }
        });
        rowCurrentStateDays.edtTxtRowValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                CURRENT_STATE_DAYS = s.toString();
                calculateDiscountTerm();
            }
        });
        row2DiscountCurrentState.edtTxtRowValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                CURRENT_STATE_PERCENTAGE = s.toString();
                calculateDiscountTerm();
            }
        });
        rowFutureStateDays.edtTxtRowValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                FUTURE_STATE_DAYS = s.toString();
                calculateDiscountTerm();
            }
        });
        rowDiscountFutureState.edtTxtRowValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                FUTURE_STATE_PERCENTAGE = s.toString();
                calculateDiscountTerm();
            }
        });

        rowAnnualSpend.edtTxtRowValue.setOnFocusChangeListener(onFocusChangeListener);
        rowCurrentStateDays.edtTxtRowValue.setOnFocusChangeListener(onFocusChangeListener);
        row2DiscountCurrentState.edtTxtRowValue.setOnFocusChangeListener(onFocusChangeListener);
        rowFutureStateDays.edtTxtRowValue.setOnFocusChangeListener(onFocusChangeListener);
        rowDiscountFutureState.edtTxtRowValue.setOnFocusChangeListener(onFocusChangeListener);

        rowAnnualSpend.edtTxtRowValue.setOnEditorActionListener(onEditorActionListener);
        rowCurrentStateDays.edtTxtRowValue.setOnEditorActionListener(onEditorActionListener);
        row2DiscountCurrentState.edtTxtRowValue.setOnEditorActionListener(onEditorActionListener);
        rowFutureStateDays.edtTxtRowValue.setOnEditorActionListener(onEditorActionListener);
        rowDiscountFutureState.edtTxtRowValue.setOnEditorActionListener(onEditorActionListener);

        //set values
        rowAnnualSpend.edtTxtRowValue.setText(CorningUtils.getConvertedValueForCurrency(ANNUAL_SPEND,false));
        rowAnnualSpend.edtTxtRowValue.setInputType(InputType.TYPE_CLASS_NUMBER);
        rowCurrentStateDays.edtTxtRowValue.setText(CURRENT_STATE_DAYS);
        rowCurrentStateDays.edtTxtRowValue.setInputType(InputType.TYPE_CLASS_NUMBER);
        row2DiscountCurrentState.edtTxtRowValue.setText(CorningUtils.getConvertedValueForDiscount(CURRENT_STATE_PERCENTAGE));
        row2DiscountCurrentState.edtTxtRowValue.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        rowFutureStateDays.edtTxtRowValue.setText(FUTURE_STATE_DAYS);
        rowFutureStateDays.edtTxtRowValue.setInputType(InputType.TYPE_CLASS_NUMBER);
        rowDiscountFutureState.edtTxtRowValue.setText(CorningUtils.getConvertedValueForDiscount(FUTURE_STATE_PERCENTAGE));
        rowDiscountFutureState.edtTxtRowValue.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
    }

    public void calculateDiscountTerm(){
        String annualSpendString = rowAnnualSpend.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$", "").replace(" ", "");
        String currentDaysString = rowCurrentStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$", "").replace(" ", "");
        String currentPctString = row2DiscountCurrentState.edtTxtRowValue.getEditableText().toString().replace("%", "").replace(" ", "").replace(",", "");
        String futureDaysString = rowFutureStateDays.edtTxtRowValue.getEditableText().toString().replace(",", "").replace("$", "").replace(" ", "");
        String futurePctString = rowDiscountFutureState.edtTxtRowValue.getEditableText().toString().replace("%", "").replace(" ", "").replace(",", "");

        double annualSpendValue;
        double currentDaysValue;
        double currentPctValue;
        double futureDaysValue;
        double futurePctValue;

        try{
            annualSpendValue = Double.parseDouble(annualSpendString);
        }catch(Exception e){
            annualSpendValue = 0;
        }

        try{
            currentDaysValue = Double.parseDouble(currentDaysString);
        }catch(Exception e){
            currentDaysValue = 0;
        }

        try{
            currentPctValue = Double.parseDouble(currentPctString);
        }catch(Exception e){
            currentPctValue = 0;
        }

        try{
            futureDaysValue = Double.parseDouble(futureDaysString);
        }catch(Exception e){
            futureDaysValue = 0;
        }

        try{
            futurePctValue = Double.parseDouble(futurePctString);
        }catch(Exception e){
            futurePctValue = 0;
        }

        double subtotalCurrent;
        double subtotalFuture;
        double discountImpactCurrent;
        double discountImpactFuture;
        double beforeImpact;
        double afterImpact;
        double netFowImpact;

        try{
            subtotalCurrent = (annualSpendValue * currentDaysValue * 0.1)/365;
        }catch (Exception e){
            subtotalCurrent = 0;
        }

        try{
            subtotalFuture = (annualSpendValue * futureDaysValue * 0.1)/365;
        }catch (Exception e){
            subtotalFuture = 0;
        }

        try{
            discountImpactCurrent = (annualSpendValue * currentPctValue)/100;
        }catch (Exception e){
            discountImpactCurrent = 0;
        }

        try{
            discountImpactFuture = (annualSpendValue * futurePctValue)/100;
        }catch (Exception e){
            discountImpactFuture = 0;
        }

        try{
            beforeImpact = subtotalCurrent + discountImpactCurrent;
        }catch (Exception e){
            beforeImpact = 0;
        }

        try{
            afterImpact = subtotalFuture + discountImpactFuture;
        }catch (Exception e){
            afterImpact = 0;
        }

        try{
            netFowImpact = afterImpact - beforeImpact;
        }catch (Exception e){
            netFowImpact = 0;
        }

        rowSubtotal1.txtVwRowValue.setText(CorningUtils.getConvertedValueForCurrency(subtotalCurrent+"", true));
        rowSubtotal2.txtVwRowValue.setText(CorningUtils.getConvertedValueForCurrency(subtotalFuture+"", true));
        rowDiscountImpact1.txtVwRowValue.setText(CorningUtils.getConvertedValueForCurrency(""+discountImpactCurrent, true));
        rowDiscountImpact3.txtVwRowValue.setText(CorningUtils.getConvertedValueForCurrency(""+discountImpactFuture, true));
        rowBeforeNetImpact.txtVwRowValue.setText(CorningUtils.getConvertedValueForCurrency(""+beforeImpact, true));
        rowAfterNetImpact.txtVwRowValue.setText(CorningUtils.getConvertedValueForCurrency(""+afterImpact, true));
        rowNetCashFlow.txtVwRowValue.setText(CorningUtils.getConvertedValueForCurrency("" + netFowImpact, true));

        //set stroke
        rowSubtotal1.txtVwRowValue.setStrokeWidth(0);
        rowSubtotal2.txtVwRowValue.setStrokeWidth(0);
        rowDiscountImpact1.txtVwRowValue.setStrokeWidth(0);
        rowDiscountImpact3.txtVwRowValue.setStrokeWidth(0);
        rowBeforeNetImpact.txtVwRowValue.setStrokeWidth(0);
        rowAfterNetImpact.txtVwRowValue.setStrokeWidth(0);

        if(netFowImpact < 0){
            rowNetCashFlow.txtVwRowValue.setTextColor(getResources().getColor(R.color.NegativeValue));
            rowNetCashFlow.txtVwRowValue.setStrokeWidth(0);
        }else{
            rowNetCashFlow.txtVwRowValue.setTextColor(getResources().getColor(R.color.PositiveValue));
            rowNetCashFlow.txtVwRowValue.setStrokeWidth(0);
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Do you want to quit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                }).setNegativeButton("No", null).show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLeftToolBar:{
                startActivity(new Intent(DiscountCalculatorActivity.this, PaymentCalculatorActivity.class));
                finish();
            }break;
            case R.id.btnRightToolBar:{
                InformationAlertView informationAlertView = new InformationAlertView(this);
                informationAlertView.show();
            }break;
        }
    }
}
